﻿using Assignment6_SqlClient.Repositories.CustomerRepo;
using Assignment6_SqlClient.Repositories.Util;
using Microsoft.Data.SqlClient;

internal class Program
{
    static void Main(string[] args)
    {
        ICustomerRepository customerRepo = new CustomerRepositoryImpl(GetConnectionString());
        DataAccessClient client = new DataAccessClient(customerRepo);
        client.DoCustomerDataAccess();
    }
    private static string GetConnectionString()
    {
        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        builder.DataSource = "localhost,1433";
        builder.InitialCatalog = "Chinook";
        builder.UserID = "sa";
        builder.Password = "Tehrekkis93";
        builder.TrustServerCertificate = true;
        return builder.ConnectionString;
    }
}
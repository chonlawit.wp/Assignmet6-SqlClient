﻿using System;
namespace Assignment6_SqlClient.Models
{
    internal readonly record struct CustomerSpender(int CustomerId, string FirstName, string LastName, decimal Count);
}


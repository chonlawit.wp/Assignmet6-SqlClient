INSERT INTO Power(Name, [Description]) VALUES
('Superhuman Strength','Strength beyond a normal human being'),
('Wallcrawling', 'Ability to walk or crawl on vertical surface'),
('Spider-Sense', 'Warns of incoming danger'),
('Flight', 'Ability to fly');


INSERT INTO SuperheroPower(SuperheroID, PowerID) VALUES
(1, 1),
(2, 1),
(3, 1),
(3, 2),
(3, 3),
(2, 4);
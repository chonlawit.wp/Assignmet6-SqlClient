CREATE TABLE [dbo].[SuperheroPower] (
    [Id]          INT IDENTITY (1, 1) NOT NULL,
    [SuperheroID] INT NOT NULL,
    [PowerID]     INT NOT NULL,
    CONSTRAINT [PK_SuperheroPower] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Power] FOREIGN KEY ([PowerID]) REFERENCES [dbo].[Power] ([Id]),
    CONSTRAINT [FK_Superhero_SP] FOREIGN KEY ([SuperheroID]) REFERENCES [dbo].[Superhero] ([Id])
);


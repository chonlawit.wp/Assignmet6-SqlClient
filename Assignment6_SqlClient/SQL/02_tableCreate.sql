CREATE TABLE [dbo].[Superhero] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]   NVARCHAR (50) NOT NULL,
    [Alias]  NVARCHAR (50) NOT NULL,
    [Origin] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Superhero] PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Assistant] (
    [Id]   INT IDENTITY (1, 1) NOT NULL,
    [Name] INT NOT NULL,
    CONSTRAINT [PK_Assistant] PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Power] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Power] PRIMARY KEY CLUSTERED ([Id] ASC)
);


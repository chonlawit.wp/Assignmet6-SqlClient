﻿using System;
using Assignment6_SqlClient.Models;

namespace Assignment6_SqlClient.Repositories.CustomerRepo
{
    internal interface ICustomerRepository : ICrudRepository<Customer, int, CustomerCountry>
    {
        public List<CustomerSpender> GetBiggestSpenders();
        public List<CustomerGenre> GetCostumerGenre(int id);
    }
}


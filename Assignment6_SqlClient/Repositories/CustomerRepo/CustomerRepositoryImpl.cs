﻿using System;
using System.Collections.Generic;

using System.Reflection.PortableExecutable;
using System.Xml.Linq;
using Assignment6_SqlClient.Models;
using Assignment6_SqlClient.Repositories.Exceptions;
using Microsoft.Data.SqlClient;
using static Azure.Core.HttpHeader;

namespace Assignment6_SqlClient.Repositories.CustomerRepo
{
    internal class CustomerRepositoryImpl : ICustomerRepository
    {
        private readonly string _connectionString;

        public CustomerRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
            DoTest();
        }

        private void DoTest()
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            Console.WriteLine("Connected");
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Add(Customer obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@FirstName", obj.FirstName);
            command.Parameters.AddWithValue("@LastName", obj.LastName);
            command.Parameters.AddWithValue("@Country", obj.Country);
            command.Parameters.AddWithValue("@PostalCode", obj.PostalCode);
            command.Parameters.AddWithValue("@Phone", obj.Phone);
            command.Parameters.AddWithValue("@Email", obj.Email);
            command.ExecuteNonQuery();
        }


        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                customers.Add(new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                    reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                    reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                    reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                    reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                    reader.IsDBNull(6) ? string.Empty : reader.GetString(6)
                    ));
            }
            return customers;
        }



        public Customer GetById(int id)
        {
            Customer customer;
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @Id";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                customer = new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                    reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                    reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                    reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                    reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                    reader.IsDBNull(6) ? string.Empty : reader.GetString(6)
                    );
            }
            else
            {
                throw new CustomerNotFoundException("No customer exists with " + id);
            }
            return customer;
        }

        public List<Customer> GetByName(string name)
        {
            //Read specific customer by name. HINT: LIKE can help for partial matches
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @Name+'%' ";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Name", name);
            using SqlDataReader reader = command.ExecuteReader();
           
            while (reader.Read())
            {
                
                customers.Add(new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                    reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                    reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                    reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                    reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                    reader.IsDBNull(6) ? string.Empty : reader.GetString(6)
                    ));
            }

            if(customers.Count == 0)
            {
                throw new CustomerNotFoundException("Customer by name");
            }

            return customers;
        }

        public void Update(Customer obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "UPDATE Customer SET FirstName = @FirstName WHERE CustomerId=@CustomerId";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@CustomerId", obj.CustomerId);
            command.Parameters.AddWithValue("@FirstName", obj.FirstName);
            command.ExecuteNonQuery();
        }

        public List<Customer> GetPage(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Limit", limit);
            command.Parameters.AddWithValue("@Offset", offset);
            using SqlDataReader reader = command.ExecuteReader();

           

            while (reader.Read())
            {

                customers.Add(new Customer(
                    reader.GetInt32(0),
                    reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                    reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                    reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                    reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                    reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                    reader.IsDBNull(6) ? string.Empty : reader.GetString(6)
                    ));
            }

            return customers;
        }

        public List<CustomerCountry> CountCustomersByCountry()
        {
            List<CustomerCountry> count = new List<CustomerCountry>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Country, COUNT(*) FROM Customer GROUP BY Country ORDER BY COUNT(*) DESC";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                count.Add(new CustomerCountry(reader.GetString(0), reader.GetInt32(1).ToString()));
                

            }
            return count;
        }

        public List<CustomerSpender> GetBiggestSpenders()
        {
            List<CustomerSpender> biggest = new();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT c.CustomerId, c.FirstName, c.LastName, t.Total FROM (SELECT Invoice.CustomerId, SUM(Total) as Total FROM Invoice GROUP BY Invoice.CustomerId) AS t INNER JOIN Customer AS c ON t.CustomerId = c.CustomerId ORDER BY Total DESC;";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
               biggest.Add(new CustomerSpender(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDecimal(3)));


            }
            return biggest;
        }

        public List<CustomerGenre> GetCostumerGenre(int id)
        {
            List<CustomerGenre> customerGenres = new();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "WITH GenreAmount AS " +
                "(SELECT Genre.Name AS Genre, COUNT(Track.TrackId) AS Amount " +
                "FROM Invoice " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                "WHERE Invoice.CustomerId = @Id " +
                "GROUP BY Genre.Name) " +
                "SELECT * FROM GenreAmount " +
                "WHERE Amount = (SELECT MAX(Amount) FROM GenreAmount) " +
                "ORDER BY Amount DESC;";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                customerGenres.Add(new(reader.GetString(0), reader.GetInt32(1)));
            }

            return customerGenres;
        }
        
    }
}

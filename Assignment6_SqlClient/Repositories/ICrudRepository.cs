﻿using System;
namespace Assignment6_SqlClient.Repositories
{
    public interface ICrudRepository<T, ID, C>
    {
        List<T> GetAll();

        T GetById(ID id);

        List<T> GetByName(string name);

        List<T> GetPage(int limit, int offset);

        void Add(T obj);

        void Update(T obj);

        void Delete(ID id);

        List<C> CountCustomersByCountry();


    }
}


﻿using System;
namespace Assignment6_SqlClient.Repositories.Exceptions
{
    internal class CustomerNotFoundException : Exception
    {
        public CustomerNotFoundException(string? message) : base(message)
        {
        }
    }
}


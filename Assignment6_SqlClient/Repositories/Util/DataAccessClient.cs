﻿using System;
using System.Text;
using Assignment6_SqlClient.Models;
using Assignment6_SqlClient.Repositories.CustomerRepo;

namespace Assignment6_SqlClient.Repositories.Util
{
    internal class DataAccessClient
    {
        private readonly ICustomerRepository _customerRepo;



        public DataAccessClient(ICustomerRepository customerRepo)
        {
            _customerRepo = customerRepo;
        }



        public void DoCustomerDataAccess()
        {
            // GetAll:
            //_customerRepo.GetAll().ForEach(c =>
            //{
            //    StringBuilder customer = new StringBuilder();
            //    customer.AppendLine("First name: " + c.FirstName);
            //    customer.AppendLine("Last name: " + c.LastName);
            //    customer.AppendLine("Country: " + c.Country);
            //    customer.AppendLine("Postal code: " + c.PostalCode);
            //    customer.AppendLine("Phone: " + c.Phone);
            //    customer.AppendLine("Email: " + c.Email);
            //    customer.AppendLine();

            //    Console.Write(customer);
            //});

            // GetById:
            //Console.Write(_customerRepo.GetById(2));

            // GetByName:
            //_customerRepo.GetByName("K").ForEach(c => Console.WriteLine(c));


            // GetPage:
            //_customerRepo.GetPage(3, 0).ForEach(c => Console.WriteLine(c));
            //_customerRepo.GetPage(3, 10).ForEach(c => Console.WriteLine(c));

            // Add:
            //_customerRepo.Add(new Customer(0,"Chonlawit", "Pettersen", "Norway", "2004", "97614861", "mail@gmail.com"));

            // Update:
            //_customerRepo.Update(new Customer(14,"Markoooooo", "Philips", "USA", "T6G 2C7", "+1 (780) 434-4554", "mphilips12@shaw.ca"));

            // CountCustomersByCountry:
            //_customerRepo.CountCustomersByCountry().ForEach(c => Console.WriteLine(c));

            _customerRepo.GetCostumerGenre(1).ForEach(r => Console.WriteLine(r));
        }
         
    }
}


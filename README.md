# Assignment 6 - Chinook

The purpose of this project is to practice writing basic SQL queries and running these through the use of .NET. It consists of nine specific queries to an existing database.

## Authors

- [@Chonlawit With-Pettersen](www.gitlab.com/chonlawit.wp)
- [@Oddbjørn Borge-Jensen](www.gitlab.com/oddbjornborg)
